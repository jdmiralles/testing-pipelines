﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XUnitTestProject1
{
    public struct Stat
    {
        int contentid;
        int start;

        public int Start { get => start; set => start = value; }
        public int Contentid { get => contentid; set => contentid = value; }
    }
}
