using Prime.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Xunit;
using XUnitTestProject1;

namespace Prime.UnitTests.Services
{
    public class PrimeService_IsPrimeShould
    {
        private readonly PrimeService _primeService;

        public PrimeService_IsPrimeShould()
        {
            _primeService = new PrimeService();
        }

        [Fact]
        public void IsPrime_InputIs1_ReturnFalse()
        {
            var result = _primeService.IsPrime(1);

            Assert.False(result, "1 should not be prime");
        }

        [Fact]
        public void ReadFile()
        {
            var xmlPlayer = GetFullPathToFile("../XMLFile1.xml");

            XDocument xml = new XDocument();
            xml = XDocument.Load(xmlPlayer);

            String playerid = xml.Root.Value;

            Assert.Equal("5874",playerid);
        }

        [Fact]
        public void SimplePlayList_WhenPlayed_ShouldReproduceInSecuence()
        {
            // Leer archivo XML generado por el Player
            /// Primera l�nea: comprobar que el contentid sea uno determinado
            ///                y que el momento de comenzar sea el correcto
            ///                
            var xmlPlayer = GetFullPathToFile("../stats_player_754_test_a1.xml");

            XDocument xml = new XDocument();
            xml = XDocument.Load(xmlPlayer);

            List<Stat> stats = new List<Stat>();

            stats = (from stat in xml.Element("stats").Elements("content")
                     select new Stat
                     {
                         Start = int.Parse(stat.Attribute("start").Value),
                         Contentid = int.Parse(stat.Attribute("id").Value)
                     }).ToList();

            
            Assert.Equal(12345, stats[0].Contentid);
            Assert.Equal(62346, stats[1].Contentid);
            Assert.Equal(32345, stats[2].Contentid);
            
        }

        internal static string GetFullPathToFile(string pathRelativeUnitTestingFile)
        {
            string folderProjectLevel = GetPathToCurrentUnitTestProject();
            string final = System.IO.Path.Combine(folderProjectLevel, pathRelativeUnitTestingFile);
            return final;
        }
        /// <summary>
        /// Get the path to the current unit testing project.
        /// </summary>
        /// <returns></returns>
        private static string GetPathToCurrentUnitTestProject()
        {
            string pathAssembly = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string folderAssembly = System.IO.Path.GetDirectoryName(pathAssembly);
            if (folderAssembly.EndsWith("/") == false) folderAssembly = folderAssembly + "/";
            string folderProjectLevel = System.IO.Path.GetFullPath(folderAssembly + "../../");
            return folderProjectLevel;
        }
    }
}
